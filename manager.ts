import constant from "./constant"
import { micro } from "./micro"
const { spawn, exec } = require('child_process')
import { clients } from "./server"

interface Info {
    pid: string,
    instance: any,
    time: Date
}

export class Managerment {
    private info: Array<Info> = []

    private process =
        (enviroment: string, params: Array<any>, services: string) => {
            const process = spawn(enviroment, params)

            process.stdout
                .on(constant.event.data, (data: any) => {
                    this.update(micro.string(data), services)
                })

            process.stdout
                .on(constant.event.error, (data: any) => {
                    this.update(micro.string(data), services)
                })
        }

    private update =
        (response: string, services: string) => {

            clients.forEach((client => client.emit(services, response)))
            console.log(response)
        }

    private services =
        () => this.process(constant.cmd.enviroment,
            constant.cmd.services, constant.services.services)

    private socket =
        () => this.process(constant.cmd.enviroment,
            constant.cmd.socket, constant.services.socket)

    private instance =
        () => this.process(constant.cmd.enviroment,
            constant.cmd.instance, constant.services.instance)

    constructor() { this.instance(); this.services(); this.socket(); }
}