const server = require('http').createServer();
const socket = require('socket.io')(server);

import constant from "./constant";
import { Managerment } from "./manager";

const managerment = new Managerment();
export const clients: Array<any> = Array()

socket.on(constant.event.connection, (client: any) => clients.push(client))

server
    .listen(constant.config.port, () => console.log(`Server manager listen on port: ${constant.config.port}`))