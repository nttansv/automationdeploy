const constant = {
    action: {
        managerLog: 'managerLog',
        servicesK: 'servicesK',
        services: 'services',
        instance: 'instance',
        socket: 'socket',
        stop: 'stop',
        kill: 'kill'
    },

    services: {
        socket: 'socket',
        instance: 'instance',
        services: 'services',
        info: 'info',
        others: 'orthers'
    },

    event: {
        connection: 'connection',
        disconnect: 'disconnect',
        message: 'message',
        error: 'error',
        sudo: 'sudo',
        data: 'data',
    },

    cmd: {
        enviroment: 'cmd.exe',
        services:
            ['/k', "d: & cd D:/Project/Code/azure-function-be & func host start --port 7071"],
        socket:
            ['/k', "c: & cd C:/Papagroup/source/nodejs-server-socket & git checkout develop -f & git pull & npm i & ts-node server.js"],
        instance:
            ['/k', "c: & cd C:/Papagroup/source/headless-instance-create & git checkout develop -f & git pull & npm i & ts-node server.ts"],
        stopServices:
            ['/k', 'TASKKILL /IM func.exe /F'],
        restart:
            'shutdown /r /t 0',
    },

    config: {
        port: 5000
    },

    process: {
        action: {
            pull: 1,
            start: 2,
            restart: 3
        },
        response: {
            ok: 200
        }
    }
}

export default constant